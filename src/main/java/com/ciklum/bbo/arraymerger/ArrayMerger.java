package com.ciklum.bbo.arraymerger;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Contains static methods for merging two sorted arrays or lists into one
 * sorted array or list respectively.
 * 
 * @author bbo
 */
public class ArrayMerger {

	public ArrayMerger() {
	}

	/**
	 * @author bbo
	 * Custom array iterator. 
	 * The difference between it and Java's built-in iterator is that this class has a method
	 * head(), which returns an object under iterator.
	 */
	static class ArrayIterator {
		int index;
		int[] array;
		
		public ArrayIterator(int[] array, int start) {
			this.array = array;
			this.index = start;
		}
		
		public int head() {
			return array[index];
		}
		
		public int next() {
			return array[index++];
		}
		
		public boolean hasNext() {
			return (index+1) < array.length;
		}
	}
	
	/**
	 * Merges 2 sorted arrays of ints and produces a sorted merged array. If
	 * both arrays contain duplicates, they all will be copied to the output.
	 * 
	 * @param array1 sorted input array to be merged
	 * @param array2 sorted input array to be merged
	 * @return sorted merged array
	 */
	public static int[] mergeSortedArrays(int[] array1, int[] array2) {

		if ((array1 == null) || (array2 == null)) {
			throw new NullPointerException("arrays shouldn't be null");
		}

		int[] resultArray = new int[array1.length + array2.length];

		// if one of the arrays is empty, then copy another array to result
		if (array1.length == 0) {
			// to prevent modification of the source arr, we copy it to result arr
			System.arraycopy(array2, 0, resultArray, 0, array2.length);
			return resultArray;
		} else if (array2.length == 0) {
			System.arraycopy(array1, 0, resultArray, 0, array1.length);
			return resultArray;
		}

		// positions of the smallest unprocessed elements in each array
		ArrayIterator iter1 = new ArrayIterator(array1, 0);
		ArrayIterator iter2 = new ArrayIterator(array2, 0);

		int i = 0; // resultArray index

		// normal case: both arrays are not empty
		// we break from the loop when we have processed both arrays
		ArrayIterator smaller, larger;
		while (true) { 
			// compare smallest unprocessed elems from both arrays
			if (iter1.head() < iter2.head()) {
				smaller = iter1;
				larger = iter2;
			} else {
				smaller = iter2;
				larger = iter1;
			}
			resultArray[i++] = smaller.head();
			
			// check for array index bound before incrementing it
			if (smaller.hasNext()) { 
				smaller.next();
			} else { // we have processed this array, now append the unprocessed part of another array
				while (larger.hasNext()) {
					resultArray[i++] = larger.head();
					larger.next();
				}
				resultArray[i++] = larger.head(); // add last elem from larger array
				break;
			}
		}

		return resultArray;
	}
	
	
	/**
	 * Merges 2 sorted lists of Comparable objects of the same type and produces a sorted merged list. 
	 * If both arrays contain duplicates, they all will be copied to the output.
	 * 
	 * @param list1 sorted input list of Comparable objects
	 * @param list2 sorted input list of Comparable objects
	 * @return sorted merged list
	 */
	public static <T extends Comparable<T>> List<T> mergeSortedLists(List<T> list1, List<T> list2) {
		
		if((list1==null) || (list2==null)) {
			throw new NullPointerException("lists shouldn't be null");
		}
		
		List<T> resultList = new ArrayList<>(list1.size()+list2.size());
		
		if(list1.isEmpty() && list2.isEmpty()) {
			return resultList;
		} else if (list1.isEmpty()) {
			resultList.addAll(list2);
			return resultList;
		} else if (list2.isEmpty()) {
			resultList.addAll(list1);
			return resultList;
		}
		
		Iterator<T> iter1 = list1.iterator();
		Iterator<T> iter2 = list2.iterator();
		T elem1 = iter1.next();
		T elem2 = iter2.next();
		
		while (true) {
			if (elem1.compareTo(elem2) < 0) {
				resultList.add(elem1);
				if (iter1.hasNext()) {
					elem1 = iter1.next();
				} else {
					while(iter2.hasNext()) {
						resultList.add(elem2);
						elem2 = iter2.next();
					}
					resultList.add(elem2);
					break;
				}
			} else {
				resultList.add(elem2);
				if (iter2.hasNext()) {
					elem2 = iter2.next();
				} else {
					while(iter1.hasNext()) {
						resultList.add(elem1);
						elem1 = iter1.next();
					}
					resultList.add(elem1);
					break;
				}
			}
		}
		
		return resultList;
	}

}
