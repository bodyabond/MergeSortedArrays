package com.ciklum.bbo.main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.ciklum.bbo.arraymerger.ArrayMerger;

public class ArrayMergerRunner {

	public static void main(String[] args) {
		int[] testArr1 = new int[]{2,5,16};
        int[] testArr2 = new int[]{1,2,7,12};
        
        int[] result = ArrayMerger.mergeSortedArrays(testArr1, testArr2);
        System.out.println("Array1:\n" + Arrays.toString(testArr1));
        System.out.println("Array2:\n" + Arrays.toString(testArr2));
        System.out.println("Merged array:\n" + Arrays.toString(result));
        
        
        List<Integer> list1 = new ArrayList<>();
        for(int i : testArr1) {
        	list1.add(i);
        }
        List<Integer> list2 = new ArrayList<>();
        for(int i : testArr2) {
        	list2.add(i);
        }
        List<Integer> resultList = ArrayMerger.mergeSortedLists(list1, list2);
        
        System.out.println("\nList1:");
        list1.forEach(elem -> System.out.format("%d ", elem));
        
        System.out.println("\nList2:");
        list2.forEach(elem -> System.out.format("%d ", elem));
        
        System.out.println("\nMerged list:");
        resultList.forEach(elem -> System.out.format("%d ", elem));
        
        double[] testArr3 = new double[]{2.1, 5.2, 16.3};
        double[] testArr4 = new double[]{1.1, 2.2, 7.3, 12.4};
        List<Double> list3 = new ArrayList<>();
        for(double d : testArr3) {
        	list3.add(d);
        }
        List<Double> list4 = new ArrayList<>();
        for(double d : testArr4) {
        	list4.add(d);
        }
        List<Double> resultList2 = ArrayMerger.mergeSortedLists(list3, list4);
        
        System.out.println("\n\nList3:");
        list3.forEach(elem -> System.out.format("%.2f ", elem));
        
        System.out.println("\nList4:");
        list4.forEach(elem -> System.out.format("%.2f ", elem));
        
        System.out.println("\nMerged list:");
        resultList2.forEach(elem -> System.out.format("%.2f ", elem));
	}

}
