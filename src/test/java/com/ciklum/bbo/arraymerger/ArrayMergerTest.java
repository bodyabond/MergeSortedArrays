package com.ciklum.bbo.arraymerger;

import static org.junit.Assert.assertArrayEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.ciklum.bbo.arraymerger.ArrayMerger;

/**
 * @author bbo
 *
 */
public class ArrayMergerTest {

	private int[] array1;
	private int[] array2;
	private int[] expected;
	
	private List<Integer> list1;
	private List<Integer> list2;
	private List<Integer> expectedList12;
	
	private List<Character> list3;
	private List<Character> list4;
	private List<Character> expectedList34;
	
	@Before
	public void setUp() throws Exception {
		array1 = new int[]{1,2,6,8,12};
		array2 = new int[]{5,8,10,11};
		expected = new int[]{1,2,5,6,8,8,10,11,12};
		
		list1 = new ArrayList<>();
		list2 = new ArrayList<>();
		expectedList12 = new ArrayList<>();
		
		for(int i : array1) {
			list1.add(i);
		}
		for(int i : array2) {
			list2.add(i);
		}
		for(int i : expected) {
			expectedList12.add(i);
		}
		
		list3 = Arrays.asList('a', 'e', 'i', 'o', 'u');
		list4 = Arrays.asList(new Character[]{'b', 'd', 'n', 'y'});
		expectedList34 = Arrays.asList('a', 'b', 'd', 'e', 'i', 'n', 'o', 'u', 'y');
	}
	
	/**
	 * Test method for {@link com.ciklum.bbo.arraymerger.ArrayMerger#mergeSortedArrays(int[], int[])}.
	 */
	@Test
	public void testMergeSortedArrays() {
		
		int[] result = ArrayMerger.mergeSortedArrays(array1, array2);
		assertArrayEquals(expected, result); // usual case
		
		
		array1 = new int[]{};
		array2 = new int[]{1,5};
		expected = new int[]{1,5};
		
		result = ArrayMerger.mergeSortedArrays(array1, array2);
		assertArrayEquals(expected, result); // when one array is empty
	}
	
	/**
	 * Test method for {@link com.ciklum.bbo.arraymerger.ArrayMerger#mergeSortedArrays(int[], int[])}.
	 */
	@Test(expected = NullPointerException.class)
	public void testMergeSortedArraysNull() {
		int[] array1 = new int[]{1,5};
		ArrayMerger.mergeSortedArrays(array1, null); // one array is null
	}

	/**
	 * Test method for {@link com.ciklum.bbo.arraymerger.ArrayMerger#mergeSortedLists(java.util.List, java.util.List)}.
	 */
	@Test
	public void testMergeSortedLists() {
		List<Integer> resultList = ArrayMerger.mergeSortedLists(list1, list2);
		assertArrayEquals(expectedList12.toArray(), resultList.toArray()); // lists of java.lang.Integer
		
		
		list1.clear();
		list2.clear();
		expectedList12.clear();
		
		list2.add(1);
		list2.add(5);
		expectedList12.addAll(list2);
		
		resultList = ArrayMerger.mergeSortedLists(list1, list2);
		assertArrayEquals(expectedList12.toArray(), resultList.toArray()); // when one list is empty
		
		
		List<Character> resultList34 = ArrayMerger.mergeSortedLists(list3, list4);
		assertArrayEquals(expectedList34.toArray(), resultList34.toArray()); // lists of java.lang.Character
	}

}
